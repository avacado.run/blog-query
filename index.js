const express = require("express");
const bodyParser = require("body-parser");
const axios = require("axios");
const cors = require("cors");

const app = express();
app.use(cors());
app.use(bodyParser.json());

const posts = {};

const handleEvent = (type, data) => {
  if (type === "PostCreated") {
    const { id, title } = data;
    posts[id] = {
      id,
      title,
      comments: [],
    };
  }

  if (type === "CommentCreated") {
    const { postId, id, content, status } = data;

    posts[postId].comments = [
      {
        id,
        content,
        status,
      },
      ...posts[postId].comments,
    ];
  }

  if (type === "CommentUpdated") {
    const { postId, id, content, status } = data;

    const comment = posts[postId].comments.find((comment) => comment.id === id);
    comment.status = status;
    comment.content = content;
  }
};

app.get("/posts", (req, res) => {
  res.send(posts);
});

app.post("/events", (req, res) => {
  const { type, data } = req.body;

  handleEvent(type, data);

  res.send({});
});

app.listen(4002, async () => {
  console.log("Listening on 4002");

  const { data } = await axios.get("http://localhost:4005/events");

  for (let event of data) {
    console.log("Processing event:", event.type);

    handleEvent(event.type, event.data);
  }
});


